import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "It works" example
    router.get { req in
        return "It works!"
    }
    
    // Basic "Hello, world!" example
    router.get("hello") { req in
        return "Hello, world!"
    }
    
    router.get("hello", String.parameter) { request -> String in
        let name = try request.parameters.next(String.self)
        return "Hello, \(name)!"
    }
    
    router.get("products") { request in
        return [Good(name: "16lb bag of Skittles", price: 16.0, types: [Taxes.tax_free.rawValue]),
            Good(name: "Walkman", price: 99.99, types: [Taxes.basic.rawValue]),
            Good(name: "bag of microwave Popcorn", price: 0.99, types: [Taxes.tax_free.rawValue]),
            Good(name: "bag of Vanilla-Hazelnut Coffee", price: 11.0, types: [Taxes.imported.rawValue]),
            Good(name: "Vespa", price: 15001.25, types: [Taxes.basic.rawValue, Taxes.imported.rawValue]),
            Good(name: "crate of Almond Snickers", price: 75.99, types: [Taxes.imported.rawValue]),
            Good(name: "Discman", price: 55.00, types: [Taxes.basic.rawValue]),
            Good(name: "bottle of Wine", price: 10.00, types: [Taxes.basic.rawValue, Taxes.imported.rawValue]),
            Good(name: "300# Bag of Fair-Trade Coffee", price: 997.99, types: [Taxes.tax_free.rawValue])]
    }

    // Example of configuring a controller
    let todoController = TodoController()
    router.get("todos", use: todoController.index)
    router.post("todos", use: todoController.create)
    router.delete("todos", Todo.parameter, use: todoController.delete)
}

enum Taxes: String {
    case tax_free = "tax_free"
    case basic = "basic"
    case imported = "imported"
}

struct Good: Content {
    let name: String
    let price: Float
    let types: [String]
}
